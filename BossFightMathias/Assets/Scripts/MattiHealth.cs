using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MattiHealth : MonoBehaviour
{
    int maxHealth = 100;
    int currentHealth;
    public AudioSource audio;
    public AudioClip mattideath;
    bool isDead;
    bool doOnce;
    public GameObject winScreen;
    public AudioClip mattiDamage;
    public AudioClip win;
    float winScreenTimer = 2f;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 50 && currentHealth > 25)
        {
            GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        if (currentHealth <= 25)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (currentHealth <= 0 && !isDead)
        {
            DoDeathStuff();
        }
        if (isDead && !doOnce)
        {
            winScreenTimer -= Time.deltaTime;
            if (winScreenTimer <= 0f && !doOnce)
            {
                winScreen.SetActive(true);
                audio.PlayOneShot(win);
                doOnce = true;
                Time.timeScale = 0;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("PlayerProjectile"))
        {
            currentHealth--;
            audio.PlayOneShot(mattiDamage);
        }
    }

    void DoDeathStuff()
    {
        isDead = true;
        GetComponent<MattiAI>().enabled = false;
        audio.PlayOneShot(mattideath);

    }
}
