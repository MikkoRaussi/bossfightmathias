using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    // Start is called before the first frame update

    public Rigidbody2D projectile;
    public float projectileSpeed;
    public AudioSource audio;
    public AudioClip shootaudio;
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
     if (Input.GetKeyDown(KeyCode.Space))
        {
            Rigidbody2D projectileClone;
            projectileClone = Instantiate(projectile, transform.position, Quaternion.identity);
            projectileClone.AddForce(Vector2.up * projectileSpeed);
            audio.PlayOneShot(shootaudio);
        }   
    }
}
