using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    // Start is called before the first frame update
    int maxHealth = 5;
    int currentHealth;
    bool isDead;
    public float blinktimer = 0.5f;
    bool tookDamage;
    bool doOnce;
    float loseScreenTimer = 2f;
    public AudioSource audio;
    public AudioClip playerDeath;
    public AudioClip playerDamage;
    public GameObject loseScreen;
    public AudioClip lose;
    Color defaultColor;

    void Start()
    {
        currentHealth = maxHealth;
        audio = GetComponent<AudioSource>();
        defaultColor = GetComponent<SpriteRenderer>().color;

    }

    // Update is called once per frame
    void Update()
    {
        if (currentHealth <= 0 && !isDead)
        {
            DoDeathStuff();
        }
        if (tookDamage)
        {
            Blink();
        }
        if (isDead && !doOnce)
        {
            loseScreenTimer -= Time.deltaTime;
            if (loseScreenTimer <= 0 && !doOnce)
            {
                loseScreen.SetActive(true);
                audio.PlayOneShot(lose);
                doOnce = true;
                Time.timeScale = 0;
            }
        }
    }

    void OnTriggerEnter2D (Collider2D col)
    {
        if (col.CompareTag("Matti"))
        {
            audio.PlayOneShot(playerDamage);
            tookDamage = true;
            currentHealth--;
        }
    }

    void DoDeathStuff()
    {
        isDead = true;
        audio.PlayOneShot(playerDeath);
        GetComponent<PlayerMovement>().enabled = false;
        GetComponent<PlayerShooting>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void Blink()
    {
        SpriteRenderer playerSprite = GetComponent<SpriteRenderer>();


        playerSprite.color = Color.red;
        blinktimer -= Time.deltaTime;
        if (blinktimer <= 0f)
        {
            playerSprite.color = defaultColor;
            blinktimer = 0.5f;
            tookDamage = false;
        }
    }

}
