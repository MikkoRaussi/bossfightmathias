using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 5f;

    private Rigidbody2D rb2d;
    private Vector2 movementVector;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        movementVector.x = Input.GetAxis("Horizontal") * movementSpeed;
    }

    void FixedUpdate()
    {
        movementVector.y = rb2d.velocity.y;
        rb2d.velocity = movementVector;
    }
}
