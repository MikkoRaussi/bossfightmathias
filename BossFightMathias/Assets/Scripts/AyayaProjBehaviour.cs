﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AyayaProjBehaviour : MonoBehaviour
{
    public GameObject projectileSpawnLocation1, projectileSpawnLocation2, projectileSpawnLocation3;
    public GameObject projEndLoc1, projEndLoc2, projEndLoc3;
    private Vector3 endLoc;

    // Start is called before the first frame update
    void Start()
    {
        projectileSpawnLocation1 = GameObject.Find("AnimeProjectileSpawn1");
        projectileSpawnLocation2 = GameObject.Find("AnimeProjectileSpawn2");
        projectileSpawnLocation3 = GameObject.Find("AnimeProjectileSpawn3");
        projEndLoc1 = GameObject.Find("AnimeProjEndLoc1");
        projEndLoc2 = GameObject.Find("AnimeProjEndLoc2");
        projEndLoc3 = GameObject.Find("AnimeProjEndLoc3");

        if (transform.position == projectileSpawnLocation1.transform.position)
        {
            endLoc = projEndLoc1.transform.position;
        }

        if (transform.position == projectileSpawnLocation2.transform.position)
        {
            endLoc = projEndLoc2.transform.position;
        }

        if (transform.position == projectileSpawnLocation3.transform.position)
        {
            endLoc = projEndLoc3.transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float step = 9 * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, endLoc, step);
        Destroy(this.gameObject, 5f);
    }
}
