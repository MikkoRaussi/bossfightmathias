﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;

public class MattiAI : MonoBehaviour
{

    public Transform animeStrikeLocation1, animeStrikeLocation2, animeStrikeLocation3;
    public GameObject animeStrikeProjectile;
    public float animeStrikeDashSpeed = 5f;
    public float bodySlamSpeed = 5f;
    public int animeStrikeSalvoCount = 3;
    public Transform[] projectileSpawnLocation;
    public GameObject[] gnome;
    public Transform playerLocation;
    public AudioClip ayaya;
    public AudioClip gnomeAudio;
    public AudioClip bodyslam;
    public AudioSource audio;

    private bool isAttacking;
    private float instatiateTimer = 0.5f;
    private float slamTimer = 3f;
    private bool isSlamming;
    private float animeAttackCount;
    private bool shouldAlignWithPlayer;
    private bool isSamePosXWithPlayer;
    private float gnomeTimer = 1f;
    private Vector3 startPos, target;
    private bool slamComplete;
    private bool shouldMove;
    private int gnomeAttackCount = 1;
    private bool readyToStartSlamming;
    int rnd;
    public Vector3 pos;


    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        startPos = transform.position;
        rnd = Random.Range(0, 3);
    }

    // Update is called once per frame
    void Update()
    {
            if (rnd == 0)
                AnimeStrike();
            if (rnd == 1)
                GnomeAttack();
            if (rnd == 2)
                BodySlam();
    }

    void AnimeStrike()
    {
        float step = animeStrikeDashSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, step);
        isAttacking = true;

        if (transform.position == startPos)
        {
            animeStrikeSalvoCount = 3;
            target = animeStrikeLocation1.position;
        }

        if (transform.position == animeStrikeLocation1.position && animeStrikeSalvoCount == 0)
        {
            animeStrikeSalvoCount = 3;
            target = animeStrikeLocation2.position;
            animeAttackCount++;
        }

        if (transform.position == animeStrikeLocation2.position && animeStrikeSalvoCount == 0)
        {
            animeStrikeSalvoCount = 3;
            target = animeStrikeLocation3.position;
            animeAttackCount++;
        }

        if (transform.position == target && animeStrikeSalvoCount != 0)
        {
            AnimeStrikeShoot();
        }

        if (transform.position == animeStrikeLocation3.position && animeStrikeSalvoCount == 0)
        {
            target = startPos;
            animeAttackCount++;
        }

        if (transform.position == startPos && animeAttackCount == 3)
        {
            isAttacking = false;
            animeAttackCount = 0;
            rnd = Random.Range(0, 3);
        }

    }
    void AnimeStrikeShoot()
    {
        if (transform.position == animeStrikeLocation1.position || transform.position == animeStrikeLocation2.position || transform.position == animeStrikeLocation3.position)
        {
            instatiateTimer -= Time.deltaTime;
            if (instatiateTimer <= 0 && animeStrikeSalvoCount != 0)
            {
                for (int i = 0; i < projectileSpawnLocation.Length; i++)
                {
                    GameObject ayayaClone = Instantiate(animeStrikeProjectile, projectileSpawnLocation[i].position, Quaternion.identity);
                    instatiateTimer = 1f;
                    audio.PlayOneShot(ayaya);
                    
                }
                animeStrikeSalvoCount--;
            }
        }
    }
    void GnomeAttack()
    {
        Color gnomeHidden = gnome[0].GetComponent<SpriteRenderer>().color;
        gnomeHidden.a = 0f;
        Color gnomeAttack = gnome[0].GetComponent<SpriteRenderer>().color;
        gnomeAttack.a = 1f;

        gnomeTimer -= Time.deltaTime;
        if (gnomeTimer <= 0 && gnomeAttackCount == 1)
        {
            gnome[0].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[0].GetComponent<BoxCollider2D>().enabled = true;
            gnome[8].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[8].GetComponent<BoxCollider2D>().enabled = true;
            gnomeAttackCount++;
            gnomeTimer = 1f;
            audio.PlayOneShot(gnomeAudio);
        }
        if (gnomeTimer <= 0 && gnomeAttackCount == 2)
        {
            gnome[1].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[1].GetComponent<BoxCollider2D>().enabled = true;
            gnome[7].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[7].GetComponent<BoxCollider2D>().enabled = true;
            gnomeTimer = 1f;
            gnomeAttackCount++;
            audio.PlayOneShot(gnomeAudio);

        }
        if (gnomeTimer <= 0 && gnomeAttackCount == 3)
        {
            gnome[2].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[2].GetComponent<BoxCollider2D>().enabled = true;
            gnome[6].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[6].GetComponent<BoxCollider2D>().enabled = true;
            gnomeTimer = 1f;
            gnomeAttackCount++;
            audio.PlayOneShot(gnomeAudio);

        }
        if (gnomeTimer <= 0 && gnomeAttackCount == 4)
        {
            gnome[3].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[3].GetComponent<BoxCollider2D>().enabled = true;
            gnome[5].GetComponent<SpriteRenderer>().color = gnomeAttack;
            gnome[5].GetComponent<BoxCollider2D>().enabled = true;
            gnomeTimer = 1f;
            gnomeAttackCount++;
            audio.PlayOneShot(gnomeAudio);

        }

        if (gnomeTimer <= 0 && gnomeAttackCount == 5)
        {
            for (int i = 0; i < gnome.Length; i++)
            {
                gnome[i].GetComponent<SpriteRenderer>().color = gnomeHidden;
                gnome[i].GetComponent<BoxCollider2D>().enabled = false;
            }
            gnomeAttackCount = 1;
            gnomeTimer = 1f;
            isAttacking = false;
            rnd = Random.Range(0, 3);
        }

    }
    void BodySlam()
    {
        float step = bodySlamSpeed * Time.deltaTime;
        isAttacking = true;

        if (!readyToStartSlamming)
        {
            pos.x = playerLocation.position.x;
            transform.position = Vector3.MoveTowards(transform.position, pos, 6f * Time.deltaTime);
            if(transform.position == pos)
            {
                readyToStartSlamming = true;
            }
        }

        if (!isSlamming && !slamComplete && readyToStartSlamming)
        {
            pos.x = playerLocation.position.x;
            transform.position = pos;
        }

        slamTimer -= Time.deltaTime;

        if (slamTimer <= 0f)
        {
            pos.x = transform.position.x;
            isSlamming = true;
        }

        if (isSlamming)
        {
            pos.y = playerLocation.position.y;
            transform.position = Vector3.MoveTowards(transform.position, pos, step);
            if (transform.position == pos)
            {
                audio.PlayOneShot(bodyslam);
                slamComplete = true;
                isSlamming = false;
            }
        }

        if (slamComplete)
        {
            slamTimer = 3f;
            pos.y = startPos.y;
            transform.position = Vector3.MoveTowards(transform.position, pos, 2f * Time.deltaTime);
            if (transform.position == pos)
            {
                shouldAlignWithPlayer = true;
            }
            if (shouldAlignWithPlayer)
            {
                pos.x = playerLocation.position.x;
                transform.position = Vector3.MoveTowards(transform.position, pos, 6f * Time.deltaTime);
                if (transform.position == pos)
                {
                    shouldAlignWithPlayer = false;
                    slamComplete = false;
                    isAttacking = false;
                    rnd = Random.Range(0, 3);
                    readyToStartSlamming = false;
                }
            }
        }
    }
}
